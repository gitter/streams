// Fetch the svg
fetch('https://upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_Antarctica.svg').then(data => {
  // data.body is a ReadableStream
  console.log(data.body)
  // so it has a reader attached:
  // https://developer.mozilla.org/en-US/docs/Web/API/ReadableStream
  let reader = data.body.getReader()
  // reads the stream to completion
  return reader.read()
}).then(processedStream => {
  console.log(processedStream)
  // decodes the stream to completion
  let decoder = new TextDecoder
  return decoder.decode(processedStream.value)
}).then(svg => {
  // console.log(svg)
  // insert svg onto page
  // document.body.insertAdjacentHTML("afterbegin", svg)
})

// fetch('https://upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_Antarctica.svg').then(data => {
//   return data.text()
// }).then(svg => {
//   document.body.insertAdjacentHTML("afterbegin", svg)
// })
