const stdin = process.openStdin()
const http = require('http')
const Future = require('ramda-fantasy').Future
const Speaker = require('speaker')
const decoder = require('lame').Decoder()
var audio

// Create the Speaker instance
const speaker = new Speaker({
    channels: 2,          // 2 channels
    bitDepth: 16,         // 16-bit samples
    sampleRate: 44100     // 44,100 Hz sample rate
})

const getData = (url) => Future((reject, resolve) =>
    http.get(url, (data) => {
        console.log(data.headers['content-type'])
        return data.statusCode === 200 ? resolve(data) : reject('error getting file')
    })
)

const play = (data) =>
    data.pipe(decoder).pipe(speaker)

const writeStreamActions = {
    pause(audio) {
        audio.cork()
    },

    resume(audio) {
        audio.uncork()
    },

    destroy(audio) {
        audio.destroy('killed.')
    },

    exit() {
        process.exit()
    }
}

stdin.on('data', function(chunk) {
    let input = chunk.toString().trim()

    if (!writeStreamActions[input]) {
        console.log('sorry this action is not understood')
    } else {
        writeStreamActions[input](audio)
    }
})

getData('http://ia802205.us.archive.org/35/items/FelaKuti/FelaKuti.mp3')
    .map(data => play(data))
    .fork(e => {console.log(e)},
        stream => {
            audio = stream
            audio.on('error', (err) => {
                console.log(err)
            })
        })

