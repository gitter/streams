const fs = require('fs')
const input = fs.createReadStream('tyrion.txt')
const { Transform } = require('stream')

const toUpper = new Transform({
    transform(chunk, encoding, callback) {
        this.push(chunk.toString().toUpperCase())
        callback()
    }
})

input.pipe(toUpper).pipe(process.stdout)

