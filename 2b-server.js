const express = require('express')
const app = express()
const fs = require('fs')

app.get('/', function (req, res) {
    let tyrion = fs.createReadStream('tyrion.txt')
    tyrion.pipe(res)
})

app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
})
