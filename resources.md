## Web API ReadableStream:
https://developer.mozilla.org/en-US/docs/Web/API/ReadableStream
## NodeJS Streams:
https://nodejs.org/dist/latest-v8.x/docs/api/stream.html
## Stream Handbook:
https://github.com/substack/stream-handbook
## Fetch:
https://jakearchibald.com/2015/thats-so-fetch/
## Streams:
https://jakearchibald.com/2016/streams-ftw/
